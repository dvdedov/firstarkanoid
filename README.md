# FirstArkanoid

- Few levels with random generation
- Two game modes
- Different bonuses

Screenshots

![Menu](https://gitlab.com/dvdedov/firstarkanoid/-/blob/master/ArkanoidMenu.png)
![Game Level](https://gitlab.com/dvdedov/firstarkanoid/-/blob/master/ArkanoidGame.png)

Developed with Unreal Engine 4
